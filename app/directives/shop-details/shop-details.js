'use strict';

angular.module('onlineShopV2App')
  .directive('shopDetails', function() {
    return {
      restrict: 'E',
      templateUrl: 'directives/shop-details/shop-details.html',
      controller: 'shopDetailsController'
    }
  })
  .controller('shopDetailsController', ['$rootScope', '$scope', '$log', function($rootScope, $scope, $log) {

    $rootScope.$on('contact-info-loaded', function() {
      $log.log($scope.facebookLink);
    });
  }]);
