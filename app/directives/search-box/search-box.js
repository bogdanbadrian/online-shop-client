'use strict';

angular
  .module('search-box', [])
  .directive('searchBox', function() {
    return {
      restrict: 'E',
      templateUrl: 'directives/search-box/search-box.html',
      controller: 'searchBoxController',
      scope: {
        onSearch: '&',
        suggestionsOn: '@'
      }
    }
  })
  .controller('searchBoxController', ['$scope', '$timeout', '$log', 'LOCAL_STORAGE_KEYS', function($scope, $timeout, $log, LOCAL_STORAGE_KEYS) {

    // public
    $scope.onQueryTextChanged = _onQueryTextChanged;
    $scope.doSearch = _doSearch;
    $scope.clearSearchBox = _clearSearchBox;
    $scope.getSearchClearIconClass = _getSearchClearIconClass;
    $scope.suggestions = [];
    $scope.clearSuggestions = _clearSuggestions;
    $scope.onSuggestionClicked = _onSuggestionClicked;

    // private
    function _doSearch() {
      if (!$scope.queryText) {
        $log.info('Empty query text. Aborting search...');
        return;
      }
      if ($scope.suggestionsOn === 'true') {
        _addSearchToCache($scope.queryText);
      }
      $timeout(function() {
        _clearSuggestions();
        $scope.onSearch({
          queryText: $scope.queryText
        });
      });
    }

    function _clearSearchBox() {
      $scope.queryText = '';
    }

    function _getSearchClearIconClass() {
      return $scope.queryText ? 'fade-in' : 'fade-out';
    }

    function _onQueryTextChanged() {
      if ($scope.queryText && $scope.suggestionsOn === 'true') {
        $timeout(function() {
          $scope.suggestions = _getSuggestionsByText($scope.queryText);
          if ($scope.suggestions.length) {
            $scope.suggestions.splice(0, 0, 'clear items');
          }
        });
      } else {
        _clearSuggestions();
      }
    }

    function _getSuggestionsByText(searchText) {
      if ($scope.oldSearches && $scope.oldSearches.length) {
        return _.filter($scope.oldSearches, function(currentSearchText) {
          return (currentSearchText.indexOf(searchText) >= 0);
        });
      }
      console.log('There are no previous searches. No suggestion to show.');
      return;
    }

    function _getOldSearches() {
      var oldSearchesString = localStorage[LOCAL_STORAGE_KEYS.OLD_SEARCHES];
      if (oldSearchesString) {
        return JSON.parse(oldSearchesString);
      }
      return '';
    }

    function _addSearchToCache(searchText) {
      if (!searchText || typeof searchText !== 'string') {
        console.warn('Could not add search to cache because is not valid. Search = ');
        console.warn(searchText);
        return;
      }
      var oldSearchesString = localStorage[LOCAL_STORAGE_KEYS.OLD_SEARCHES],
        oldSearchesArray;

      if (oldSearchesString) {
        oldSearchesArray = JSON.parse(oldSearchesString);
        if (oldSearchesArray.indexOf(searchText) >= 0) {
          console.log('Search text already in cache. Not adding it anymore. Aborting...');
          return;
        }
        oldSearchesArray.push(searchText);
        oldSearchesString = JSON.stringify(oldSearchesArray);
        localStorage[LOCAL_STORAGE_KEYS.OLD_SEARCHES] = oldSearchesString;
      } else {
        oldSearchesArray = [];
        oldSearchesArray.push(searchText);
        oldSearchesString = JSON.stringify(oldSearchesArray);
        localStorage[LOCAL_STORAGE_KEYS.OLD_SEARCHES] = oldSearchesString;
      }
    }

    function _clearSuggestions() {
      $scope.suggestions = [];
    }

    function _init() {
      $scope.oldSearches = _getOldSearches();
    }

    function _onSuggestionClicked(clickedSuggestion) {
      if (clickedSuggestion === $scope.suggestions[0]) {
        console.log('Clearing suggestions...');
        _deleteSuggestions();
        _clearSuggestions();
        return;
      }
      if (clickedSuggestion === $scope.queryText) {
        _clearSuggestions();
        return;
      }
      $scope.queryText = clickedSuggestion;
      _onQueryTextChanged();
    }

    function _deleteSuggestions() {
      console.log('Deleting suggestions...');
      localStorage[LOCAL_STORAGE_KEYS.OLD_SEARCHES] = "";
    }

    _init();

  }]);
