'use strict';

angular.module('onlineShopV2App')
  .directive('modalDialog', function() {
  return {
    restrict: 'E',
    scope: {
      show: '='
    },
    replace: true, // Replace with the template below
    link: function(scope, element, attrs) {
      scope.dialogStyle = {};
      if (attrs.width)
        scope.dialogStyle.width = attrs.width;
      if (attrs.height)
        scope.dialogStyle.height = attrs.height;
      scope.hideModal = function() {
        scope.show = false;
      };
    },
    controller: function($scope, $state, adminService) {

    },
    templateUrl: 'directives/modalDialog/modal-dialog.html'
  };
});