'use strict';

angular.module('onlineShopV2App')
  .directive('addToWishList', function() {
    return {
      restrict: 'E',
      templateUrl: 'directives/addToWishList/addToWishList.html',
      controller: 'addToWishListController'
    }
  })
  .controller('addToWishListController', ['$scope', '$log', 'wishListService', function($scope, $log, wishListService) {

    $scope.addToFavorite = _addToFavorite;

    function _addToFavorite() {
      wishListService.addProductToWishList($scope.product._id);
      $scope.alreadyAddedToWishList = true;
      $log.info('Product added to wishList.');
    }

    function init() {
      var wishListProductIds = wishListService.getProductIdsFromWishList();
      if (wishListProductIds.indexOf($scope.product._id) >= 0) {
        $scope.alreadyAddedToWishList = true;
      }
    }

    init(); // directive is loaded only if product exists. Therefore, $scope.product is defined
  }]);
