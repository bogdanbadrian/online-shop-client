'use strict';

angular.module('onlineShopV2App')
    .directive('price', function() {
        return {
          restrict: 'E',
          scope: {
            oldPrice: '@',
            newPrice: '@'
          },
          templateUrl: 'directives/price/price.html',
          controller: 'priceController'
        }
    })
    .controller('priceController', ['$scope', function($scope){

      $scope.getOldPrice = function() {
        return $scope.oldPrice ? $scope.oldPrice + " Lei" : '';
      };

      $scope.getNewPrice = function() {
        return $scope.newPrice ? $scope.newPrice + " Lei" : '';
      };

      $scope.getDiscountValue = function() {
        return $scope.newPrice && $scope.oldPrice ? 'Reducere ' + getDiscountSubtractionValue() + ' Lei' : '';
      };

      $scope.isDiscountCase = function() {
        return ($scope.newPrice && $scope.oldPrice) ? $scope.newPrice < $scope.oldPrice : '';
      };

      $scope.getStyleForPrice = function() {
        return $scope.isDiscountCase() ? 'danger-text' : '';
      };

      var getDiscountSubtractionValue = function() {
        return (($scope.oldPrice * 10 - $scope.newPrice * 10) / 10);
      };

    }]);
