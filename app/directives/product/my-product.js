'use strict';

angular.module('onlineShopV2App')
    .directive('myProduct', function() {
        return {
          restrict: 'E',
          scope: {
            product:'='
          },
          templateUrl: 'directives/product/my-product.html',
          link: function(scope, element, attrs) {
            var temp = scope;
          },
          controller: 'myProductController'
        }
    })
    .controller('myProductController', ['$scope', function($scope){
      $scope.getProductPrice = function() {
        return $scope.product.price.toString() + ' lei';
      };

      $scope.getOldProductPrice = function() {
        return $scope.product.oldPrice.toString() + ' lei';
      }

      $scope.getPriceCSSClasses = function() {
        return $scope.discountCase ? 'discount-price' : 'product-price small-top-padding';
      };

      function checkForDiscount() {
        if ($scope.product.oldPrice > $scope.product.price) {
          $scope.discountCase = true;
        }
      }
      checkForDiscount();
    }]);
