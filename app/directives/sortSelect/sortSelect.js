'use strict';

angular
  .module('sortSelect', [])
  .directive('sortSelect', function() {
    return {
      restrict: 'E',
      templateUrl: 'directives/sortSelect/sortSelect.html',
      controller: 'sortSelectController',
      scope: {
        onSortOptionChanged: '&'
      }
    }
  })
  .controller('sortSelectController', ['$scope', '$timeout', 'SORT_KEYS', function($scope, $timeout, SORT_KEYS) {
    $scope.predicate = '';
    $scope.reverse = '';
    $scope.options = [SORT_KEYS.ASCENDING_PRICE, SORT_KEYS.DESCENDING_PRICE, SORT_KEYS.ASCENDING_NAME, SORT_KEYS.DESCENDING_NAME];
    $scope.selectedOption = '';

    $scope.onSelectedOptionChanged = function() {
      console.log('Sorting selected option changed to:' + $scope.selectedOption);
      _onSortOptionSelected($scope.selectedOption);
    };

    function _setSortPredicate(predicate) {
      $scope.predicate = predicate;
    }

    function _setSortingOrder(reverse) {
      $scope.reverse = reverse;
    }

    function _onSortOptionSelected(option) {
      switch (option) {
        case SORT_KEYS.ASCENDING_NAME:
          _setSortPredicate('name');
          _setSortingOrder(false);
          break;
        case SORT_KEYS.ASCENDING_PRICE:
          _setSortPredicate('price');
          _setSortingOrder(false);
          break;
        case SORT_KEYS.DESCENDING_NAME:
          _setSortPredicate('name');
          _setSortingOrder(true);
          break;
        case SORT_KEYS.DESCENDING_PRICE:
          _setSortPredicate('price');
          _setSortingOrder(true);
          break;
      }
      $timeout(function() {
        $scope.onSortOptionChanged({
          predicate: $scope.predicate,
          reverse: $scope.reverse
        });
      });
    }

  }]);
