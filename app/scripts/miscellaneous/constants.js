angular.module('onlineShopV2App')
  .constant('API', {
    DOMAIN: 'https://shopwebapi.herokuapp.com/api',
    GET_LANGUAGES: '/languages',
    GET_PRODUCT_BY_ID: '/products/',
    ADMIN: '/admin',
    SEARCH: '/search/',
    POST_BANNERS: '/banners/',
    POST_CATEGORIES: '/categories/',
    POST_PRODUCTS: '/products/',
    UPLOADS: 'http://localhost:8000/uploads/',
    GET_ALL_PRODUCTS: '/products/',
    GET_RECOMMENDED_PRODUCTS :'/products/recommended',
    SEND_MAIL_TO_ADMIN: '/admin/credentialstoadmin',
    SEND_MAIL_TO_OTHERS: '/admin/credentialstoothers'
  })
  .constant('LOCAL_STORAGE_KEYS', {
  	WISH_LIST: 'wishList',
    OLD_SEARCHES: 'oldSearches',
    RECENT_VIEWED_PRODUCTS: 'recentViewedProducts'
  })
  .constant('SORT_KEYS', {
    ASCENDING_PRICE: 'pret, crescator',
    ASCENDING_NAME: 'nume, crescator',
    DESCENDING_PRICE: 'pret, descrescator',
    DESCENDING_NAME: 'nume, descrescator',
  });
