(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('searchCtrl', searchCtrl);

  function searchCtrl($scope, $log, $timeout, $stateParams, $filter, applicationService) {

    // public
    $scope.onSortOptionChanged = _onSortOptionChanged;
    $scope.searchResultsText = 'Niciun rezultat...';
    $scope.loading = true;
    // private
    var queryText = $stateParams.queryText,
      orderBy = $filter('orderBy');

    function _doSearch(queryText) {
      $timeout(function() {
        applicationService.searchProducts(queryText, function(searchResults) {
          $timeout(function() {
            $scope.loading = false;
            if (searchResults && _.isArray(searchResults)) {
              $scope.searchResults = searchResults;
            } else {
              $log.warn('The result of the search is not an array. Cannot display anything!');
            }
          });
        }, function() {
          $scope.searchResultsText = 'Ne pare rau, dar a aparut o eroare in sistem, iar cautarea dvs. nu a putut fi efectuata. Daca situatia persista, va rugam contactati administratorul.';
        });
      }, 3000);
    }

    function _reorderProducts(products, predicate, reverse) {
      var innerProducts = products ? products : $scope.searchResults;
      $scope.searchResults = orderBy(innerProducts, predicate, reverse);
    }

    function _onSortOptionChanged(predicate, reverse) {
      _reorderProducts(null, predicate, reverse);
    }

    _doSearch(queryText);

  }

  searchCtrl.$inject = ['$scope', '$log', '$timeout', '$stateParams', '$filter', 'applicationService'];

})();
