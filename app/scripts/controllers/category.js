(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('categoryCtrl', categoryCtrl);

  function categoryCtrl($scope, $stateParams, $filter, categoriesService) {

    // public
    $scope.categoryId = $stateParams.selectedCategory;
    $scope.productsLoaded = false;
    $scope.products = [];
    $scope.category = {};
    $scope.onSortOptionChanged = _onSortOptionChanged;

    // private
    var orderBy = $filter('orderBy');

    function _loadProducts() {
      $scope.category = categoriesService.getCategoryById($scope.categoryId);
      if ($scope.category) {
        categoriesService.getCategoryProducts($scope.category,
          function(products) {
            _reorderProducts(products);
            $scope.productsLoaded = true;
          });
      } else {
        console.warn('For some reasons, categoryId is not a valid category id, and is in fact the id of a product.');
      }
    }

    function _init() {
      if (categoriesService.categoriesLoaded()) {
        _loadProducts();
      } else {
        categoriesService.onCategoriesLoaded(function() {
          _loadProducts();
        });
      }
    }

    function _reorderProducts(products, predicate, reverse) {
      var innerProducts = products ? products : $scope.products;
      $scope.products = orderBy(innerProducts, predicate, reverse);
    }

    function _onSortOptionChanged(predicate, reverse) {
      _reorderProducts(null, predicate, reverse);
    }

    _init();

  }
  categoryCtrl.$inject = ['$scope', '$stateParams', '$filter', 'categoriesService'];
})();
