(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('adminCtrl', adminCtrl);

  function adminCtrl($scope, $state, $timeout, vcRecaptchaService, adminService) {
    // public
    $scope.login = login;
    $scope.sendPasswordMailToAdmin = sendPasswordMailToAdmin;
    $scope.sendPasswordMailToOthers = sendPasswordMailToOthers;
    $scope.showCaptchaSection = false;
    $scope.showRecoverSection = false;
    $scope.refreshImages = refreshImages;
    $scope.imageUrls = [];

    $scope.captchaKey = '6LdvAhYTAAAAAHEfYcvrhDu8Xuvgx5yv7iYLAL_w';

    $scope.setResponse = function(response) {
      $scope.response = response;
    };

    $scope.setWidgetId = function(widgetId) {
      $scope.widgetId = widgetId;
    };

    $scope.cbExpiration = function() {
      $scope.response = null;
    };

    $scope.submit = function() {
      var valid = false;

      if ($scope.response) {
        $scope.showRecoverSection = true;
        $scope.showCaptchaSection = false;
        $scope.response = null;
        $scope.displayWarningMessageForSendToAdmin = false;
        $scope.displayWarningMessageForSendToOthers = false;
        $scope.displayEmergencyButton = false;
      } else {
        vcRecaptchaService.reload($scope.widgetId);
      }
    };

    $scope.recoverPassword = function() {
      $scope.showCaptchaSection = true;
    };

    function sendPasswordMailToAdmin() {
      adminService.sendPasswordMailToOthers(function(result) {
        $scope.displayWarningMessageForSendToAdmin = true;
        $timeout(function() {
          $scope.displayEmergencyButton = true;
        }, 5000);
      }, function() {

      });
    }

    function sendPasswordMailToOthers() {
      adminService.sendPasswordMailToOthers(function(result) {
        $scope.displayWarningMessageForSendToOthers = true;
        $timeout(function() {
          $scope.showCaptchaSection = false;
          $scope.showRecoverSection = false;
        }, 5000);
      }, function() {

      });
    }

    function refreshImages() {
      adminService.getImageUrls(function(imageUrls) {
        $scope.imageUrls = imageUrls.data;
      });  
    }

    // private
    function login() {
      $scope.showFailedLogin = false;
      adminService.login($scope.password, function(result) {
        if (result) {
          $scope.showFailedLogin = false;
          $state.go('administration');
        }
      }, function() {
        $scope.showFailedLogin = true;
      });
    }

    function _init() {
      adminService.isAuthorized(function(isAuthorized) {
        $scope.isAuthorized = isAuthorized;
      });
    }

    _init();
  }
  adminCtrl.$inject = ['$scope', '$state', '$timeout', 'vcRecaptchaService', 'adminService'];
})();
