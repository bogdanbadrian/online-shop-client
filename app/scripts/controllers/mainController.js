(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('mainCtrl', mainCtrl);

  function mainCtrl($rootScope, $scope, $timeout, $state, categoriesService, applicationService, API, productsService) {

    // public
    $scope.categoriesLoaded = false;
    $scope.searchResults = [];
    $scope.doSearch = _doSearch;
    $scope.categories = [];
    $rootScope.$state = $state;
    $scope.openFacebook = _openFacebook;
    $scope.getDropDownVisibility = _getDropDownVisibility;
    $scope.hideDropDown = _hideDropDown;
    $scope.showDropDown = _showDropDown;

    // private
    var dropDownVisibility = false;

    function _doSearch(queryText) {
      console.log('doing search ...' + queryText);
      $state.go('shop.search', {
        queryText: queryText
      });
    }

    function _showDropDown() {
      dropDownVisibility = true;
    }

    function _hideDropDown() {
      dropDownVisibility = false;
    }

    function _getDropDownVisibility() {
      return dropDownVisibility ? 'in' : 'collapse';
    }

    function _openFacebook() {
      window.open($scope.facebookLink, '_blank');
    }

    function loadCategories() {
      if (categoriesService.categoriesLoaded()) {
        categoriesService.getCategories(function(categories) {
          $scope.categories = categories;
          $scope.categoriesLoaded = true;
        });
      } else {
        categoriesService.onCategoriesLoaded(function(categories) {
          $timeout(function() {
            $scope.categories = categories;
            $scope.categoriesLoaded = true;
            $scope.$apply();
          });
        });
      }
    }

    function loadContactInfo() {
      applicationService.getContactData(
        function(contactData) {
          $timeout(function() {
            $scope.email = contactData.email;
            $scope.phoneNumber = contactData.phone;
            $scope.facebookLink = contactData.facebook;
            $scope.logoLink = contactData.logo;
            $rootScope.$emit('contact-info-loaded');
          });
        },
        function() {});
    }

    function loadRecommendedProducts() {
      productsService.getRecommendedProducts(function(products) {
        $timeout(function() {
          products = _.sortBy(products, 'orderNumber');
          $scope.recommendedProducts = products;
        });
      }, function() {});
    }

    function _init() {
      loadContactInfo();
      loadCategories();
      loadRecommendedProducts();
    }
    _init();
  }
  mainCtrl.$inject = ['$rootScope', '$scope', '$timeout', '$state', 'categoriesService', 'applicationService', 'API', 'productsService'];
})();
