(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('bannersController', bannersController);

  function bannersController($scope, applicationService, bannersService, helperService) {

    //public
    $scope.bannersLoaded = false;
    $scope.onClick = _onClick;
    $scope.banners = [];
    $scope.bannersLoaded = false;


    // private
    function _onClick(bannerId) {
      var clickedBanner = _getBannerById(bannerId);
      if (clickedBanner && clickedBanner.targetUrl) {
        window.open(clickedBanner.targetUrl, '_blank');
      } else {
        console.warn('Banner does not have a website to open.');
      }
    }

    function _init() {
      if (!applicationService.getCurrentLanguage()) {
        applicationService.registerObserverCallback(_onLanguageLoaded);
      } else {
        _getBanners();
      }
    }

    function _onLanguageLoaded() {
      _getBanners();
    }

    function _getBannerById(id) {
      return _.find($scope.banners, function(currentBanner) {
        return currentBanner._id === id;
      });
    }

    function _getBanners() {
      function setBannersIndexes(banners) {
        for (var i = 0; i < banners.length; i++) {
          banners[i].index = i;
        }
      }

      function addHttpToTargetUrlIfNeeded(banners) {
        for (var i = 0; i < banners.length; i++) {
          if (banners[i].targetUrl) {
            banners[i].targetUrl = helperService.addHttpToTargetUrlIfNeeded(banners[i].targetUrl);
          }
        }
      }

      function filterActiveBanners(banners) {
        return _.filter(banners, function(banner) {
          return banner.isActive;
        });
      }

      bannersService.getBanners(function(banners) {
        if ($scope.banners && $scope.banners.length) {
          return;
        }
        banners = filterActiveBanners(banners);
        setBannersIndexes(banners);
        addHttpToTargetUrlIfNeeded(banners);
        $scope.banners = banners;
        $scope.bannersLoaded = true;
      });
    }

    _init();
  }
  bannersController.$inject = ['$scope', 'applicationService', 'bannersService', 'helperService'];
})();
