(function() {
  'use strict';

  var AboutCtrl = function() {};
  
  angular
    .module('onlineShopV2App')
    .controller('AboutCtrl', AboutCtrl);
})();
