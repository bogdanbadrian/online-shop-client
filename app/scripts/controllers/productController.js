(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('productCtrl', productCtrl);

  function productCtrl($scope, $log, $stateParams, categoriesService, productsService, cachedProductsService, recentViewedProductsService) {

    // public
    $scope.selectedProduct = $stateParams.selectedProduct;
    $scope.parameter = {};
    $scope.onSmallPictureHover = _onSmallPictureHover;
    $scope.getProductPrice = _getProductPrice;
    $scope.recentViewedProducts = [];

    // private
    var recentViewedProductsLoaded = false,
      recentViewedProductsNotOrdered = [],
      recentViewedProductsIds = recentViewedProductsService.getProducts();

    function _onSmallPictureHover(index) {
      var pictures = _getPictureUrlByIndex($scope.product, index);
      if (_.isArray(pictures) && pictures.length) {
        $scope.mainPicture = pictures[0].pictureUrl;
      }
    }

    function _getProductPrice() {
      return $scope.product && $scope.product.price ? $scope.product.price + ' lei' : '';
    }

    function _loadProduct() {
      var product = cachedProductsService.getProductById($scope.selectedProduct);
      if (product) {
        _getProductByIdCallback(product);
        if (recentViewedProductsLoaded) {
          recentViewedProductsService.addProduct($scope.product._id);
        }
      } else {
        productsService.getProductById($scope.selectedProduct, _getProductByIdCallback);
      }
    }

    function _refreshPicturesForGallery(productPictures) {
      $scope.productPicturesGallery = productPictures.map(function(currentPicObject) {
        return {
          thumb: currentPicObject.pictureUrl,
          img: currentPicObject.pictureUrl,
          description: '',
          index: currentPicObject.index
        };
      });
    }

    function _getPictureUrlByIndex(product, index) {
      return _.where(product.otherPicturePaths, {
        index: index
      });
    }

    function _setPictureIndex(product) {
      for (var i = product.otherPicturePaths.length - 1; i >= 0; i--) {
        product.otherPicturePaths[i] = {
          index: i,
          pictureUrl: product.otherPicturePaths[i].pictureUrl ? product.otherPicturePaths[i].pictureUrl : product.otherPicturePaths[i]
        };
      }
    }

    function _loadRecentViewedProducts() {
      var currentProduct;
      if (!recentViewedProductsIds.length) {
        recentViewedProductsLoaded = true;
        return;
      }
      for (var i = recentViewedProductsIds.length - 1; i >= 0; i--) {
        currentProduct = cachedProductsService.getProductById(recentViewedProductsIds[i]);
        if (!currentProduct) {
          productsService.getProductById(recentViewedProductsIds[i], _getRecentViewedProductByIdCallback);
        } else {
          _getRecentViewedProductByIdCallback(currentProduct);
        }
      }
    }

    function _getRecentViewedProductByIdCallback(receivedProduct) {
      recentViewedProductsNotOrdered.push(receivedProduct);
      if (recentViewedProductsNotOrdered.length === recentViewedProductsIds.length) {
        $log.info('All recent viewed products loaded. Started ordering them...');
        recentViewedProductsLoaded = true;
        $scope.recentViewedProducts = recentViewedProductsNotOrdered;
        if ($scope.product) {
          recentViewedProductsService.addProduct($scope.product._id);
        }
      }
    }

    function _init() {
      if (categoriesService.categoriesLoaded()) {
        _loadProduct();
      } else {
        categoriesService.onCategoriesLoaded(function() {
          _loadProduct();
        });
      }
      _loadRecentViewedProducts();
    }

    function _getProductByIdCallback(product) {
      $scope.product = product;
      $scope.mainPicture = $scope.product.mainPicturePath;
      _refreshPicturesForGallery($scope.product.otherPicturePaths);
      if (recentViewedProductsLoaded) {
        recentViewedProductsService.addProduct($scope.product._id);
      }
    }

    _init();
  }
  productCtrl.$inject = ['$scope', '$log', '$stateParams', 'categoriesService', 'productsService', 'cachedProductsService', 'recentViewedProductsService'];
})();
