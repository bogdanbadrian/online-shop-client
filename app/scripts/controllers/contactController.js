(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('contactCtrl', contactCtrl);

  function contactCtrl($scope, $log, $timeout, applicationService, helperService) {

    // public
    $scope.openFacebook = _openFacebook;

    // private

    function _openFacebook() {
      $log.info('Opening facebook link...');
      window.open($scope.facebookLink, '_blank');
    }

    function init() {
    applicationService.getContactData(
      function(contactData) {
        $timeout(function(){
          $scope.email = contactData.email;
          $scope.description = contactData.description;
          $scope.phoneNumber = contactData.phone;
          $scope.name = contactData.name;
          $scope.facebookLink = contactData.facebook;
          if ($scope.facebookLink) {
            $scope.facebookLink = helperService.addHttpToTargetUrlIfNeeded($scope.facebookLink);
          }
          var coordinates = contactData.coordinates;
        }, 1);
      },
      function(err) {
        $scope.err = err;
      });
    }

    init();
  }

  contactCtrl.$inject = ['$scope', '$log', '$timeout', 'applicationService', 'helperService'];
})();
