(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .controller('wishListCtrl', wishListCtrl);

  function wishListCtrl($scope, wishListService, cachedProductsService, productsService) {

    // public
    $scope.wishListIds = wishListService.getProductIdsFromWishList();
    $scope.wishList = [];
    $scope.deleteProduct = _deleteProduct;

    // private
    function _deleteProduct(productId) {
      wishListService.removeProductFromWishList(productId);
      for (var i = 0; i < $scope.wishList.length; i++) {
        if ($scope.wishList[i]._id === productId) {
          break;
        }
      }
      $scope.wishList.splice(i, 1);
    }

    function _init() {
      var i, product;
      for (i = 0; i < $scope.wishListIds.length; i++) {
        product = cachedProductsService.getProductById($scope.wishListIds[i]);
        if (!product) {
          productsService.getProductById($scope.wishListIds[i], _getProductByIdCallback);
        } else {
          _getProductByIdCallback(product);
        }
      }
      $scope.productsLoaded = true;
    }

    function _getProductByIdCallback(receivedProduct) {
      $scope.wishList.push(receivedProduct);
    }

    _init();
  }
  wishListCtrl.$inject = ['$scope', 'wishListService', 'cachedProductsService', 'productsService'];
})();
