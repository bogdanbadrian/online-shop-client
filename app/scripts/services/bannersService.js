(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('bannersService', bannersService);

  function bannersService($http, applicationService, API) {
    // public
    var service = {
      registerObserverCallback: _registerObserverCallback,
      getBanners: _getBanners,
      updateBanner: _updateBanner,
      deleteBanner: _deleteBanner,
      createBanner: _createBanner
    };

    // private
    var _banners = [],
      observerCallbacks = [],
      _language;

    function _getBanners(successCallback, errorCallback) {
      if (_.isArray(_banners) && _banners.length) {
        if (!_.isFunction(successCallback)) {
          console.warn('Could not get categories because callback are not functions.');
          return;
        }
        successCallback(_banners);
      }
      if (!_language) {
        _language = applicationService.getCurrentLanguage();
      }
      $http.get(_language.links.banners)
        .success(function(banners) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not get banners because callback are not functions.');
            return;
          }
          _banners = banners;
          successCallback(banners);
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not get banners because callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _updateBanner(banner, successCallback, errorCallback) {
      $http.patch(banner.links.self, banner)
        .success(function(banner) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update banner because success callback are not functions.');
            return;
          }
          _onUpdate(banner);
          successCallback(banner);
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update banner because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _deleteBanner(banner, successCallback, errorCallback) {
      $http.delete(banner.links.self)
        .success(function() {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update banner because success callback are not functions.');
            return;
          }
          _onDelete(banner);
          successCallback();
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update banner because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _createBanner(banner, successCallback, errorCallback) {
      banner.language_id = applicationService.getCurrentLanguage()._id;
      $http.post(API.DOMAIN + API.POST_BANNERS, banner)
        .success(function(banner) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update banner because success callback are not functions.');
            return;
          }
          _onCreate(banner);
          successCallback(banner);
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update banner because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _orderBanners() {
      _banners.sort(function(a, b) {
        if (a.isActive === b.isActive) {
          return a.orderNumber - b.orderNumber;
        } else if (a.isActive) {
          return -1;
        }
        return 1;
      });
    }

    var _onCreate = function(banner) {
      _banners.push(banner);
      _orderBanners();
      _notifyObserverCallbacks(_banners);
    };

    var _onDelete = function(banner) {
      for (var i = 0; i < _banners.length; i++) {
        if (_banners[i]._id === banner._id) {
          _banners.splice(i, 1);
          break;
        }
      }
      _notifyObserverCallbacks(_banners);
      if (banner.picturePath && banner.picturePath.indexOf(API.UPLOADS) > -1) {
        _deleteImage(banner.picturePath);
      }
    };

    var _onUpdate = function(banner) {
      for (var i = 0; i < _banners.length; i++) {
        if (_banners[i]._id === banner._id) {
          if (_banners[i].picturePath !== banner.picturePath) {
            if (_banners[i].picturePath.indexOf(API.UPLOADS) > -1) {
              _deleteImage(_banners[i].picturePath);
            }
          }
          _banners[i] = banner;
          break;
        }
      }
      _orderBanners();
      _notifyObserverCallbacks(_banners);
    };

    var _deleteImage = function(url) {
      $http.delete(url)
        .success(function() {
          console.log('Picture deleted.');
        })
        .error(function(err) {
          console.error(err);
        });
    };

    // OBSERVER PATTERN
    function _registerObserverCallback(callback) {
      if (_.isFunction(callback)) {
        observerCallbacks.push(callback);
      }
    }

    function _notifyObserverCallbacks(data) {
      for (var i = 0; i < observerCallbacks.length; i++) {
        observerCallbacks[i](data);
      }
    }

    return service;
  }
  bannersService.$inject = ['$http', 'applicationService', 'API'];
})();
