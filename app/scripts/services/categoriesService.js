(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('categoriesService', categoriesService);

  function categoriesService($http, API, applicationService, productsService) {

    // public
    var service = {
      getCategories: _getCategories,
      getCategoryById: _getCategoryById,
      getCategoryProducts: _getCategoryProducts,
      updateCategory: _updateCategory,
      deleteCategory: _deleteCategory,
      createCategory: _createCategory,
      onCategoriesLoaded: _registerObserverCallback,
      categoriesLoaded: _categoriesLoaded,
      init: _init
    };

    // private
    var _language = applicationService.getCurrentLanguage(),
      observerCallbacks = [],
      _categories;

    function _getCategories(successCallback, errorCallback) {
      if (_categories) {
        if (!_.isFunction(successCallback)) {
          console.warn('Could not get categories because callback are not functions.');
          return;
        }
        successCallback(_categories);
      }
      $http.get(_language.links.categories)
        .success(function(categories) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not get categories because callback are not functions.');
            return;
          }
          _categories = categories;
          successCallback(categories);
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not get categories because callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function onLanguageLoaded(language) {
      _language = language;
      _getCategories(function(categories) {
        _notifyObserverCallbacks(categories);
      });
    }

    function _getCategoryById(id) {
      return _categories.filter(function(category) {
        return category._id === id;
      })[0];
    }

    function _getCategoryProducts(category, successCallback, errorCallback) {
      productsService.getProducts(category.links.products, successCallback, errorCallback);
    }

    function _updateCategory(category, successCallback, errorCallback) {
      $http.patch(category.links.self, category)
        .success(function(category) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update category because success callback are not functions.');
            return;
          }
          _onUpdate(category);
          successCallback();
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update category because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _deleteCategory(category, successCallback, errorCallback) {
      $http.delete(category.links.self)
        .success(function() {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update category because success callback are not functions.');
            return;
          }
          _onDelete(category);
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update category because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _createCategory(category, successCallback, errorCallback) {
      category.language_id = applicationService.getCurrentLanguage()._id;
      $http.post(API.DOMAIN + API.POST_CATEGORIES, category)
        .success(function(category) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update category because success callback are not functions.');
            return;
          }
          _onCreate(category);
          successCallback();
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update category because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _orderCategories() {
      _categories.sort(function(a, b) {
        if (a.isActive === b.isActive) {
          return a.orderNumber - b.orderNumber;
        } else if (a.isActive) {
          return -1;
        }
        return 1;
      });
    }

    var _onCreate = function(category) {
      _categories.push(category);
      _orderCategories();
      _notifyObserverCallbacks(_categories);
    };

    var _onDelete = function(category) {
      for (var i = 0; i < _categories.length; i++) {
        if (_categories[i]._id === category._id) {
          _categories.splice(i, 1);
          break;
        }
      }
      _notifyObserverCallbacks(_categories);
    };

    var _onUpdate = function(category) {
      for (var i = 0; i < _categories.length; i++) {
        if (_categories[i]._id === category._id) {
          _categories[i] = category;
          break;
        }
      }
      _orderCategories();
      _notifyObserverCallbacks(_categories);
    };

    // OBSERVER PATTERN
    function _registerObserverCallback(callback) {
      if (_.isFunction(callback)) {
        observerCallbacks.push(callback);
      }
    }

    function _notifyObserverCallbacks(data) {
      for (var i = 0; i < observerCallbacks.length; i++) {
        observerCallbacks[i](data);
      }
    }

    function _categoriesLoaded() {
      return !!_categories;
    }

    function _init() {
      if (applicationService.languagesLoaded()) {
        onLanguageLoaded(applicationService.getCurrentLanguage());
      } else {
        applicationService.registerObserverCallback(onLanguageLoaded);
      }
    }
    return service;
  }
  categoriesService.$inject = ['$http', 'API', 'applicationService', 'productsService'];
})();
