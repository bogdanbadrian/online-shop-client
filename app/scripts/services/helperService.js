(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('helperService', helperService);

  function helperService() {
    
    // public
    var service = {
      addHttpToTargetUrlIfNeeded: _addHttpToTargetUrlIfNeeded,
      removeDuplicates: _removeDuplicates
    };

    function _addHttpToTargetUrlIfNeeded(url) {
      return url && url.indexOf('http://') === -1 ? 'http://' + url : '';
    }

    function _removeDuplicates(array) {
      var toReturn = [],
          firstOccurrence,
          lastOccurrence;
      for (var i = 0; i < array.length; i++) {
        firstOccurrence = array.indexOf(array[i]);
        lastOccurrence = array.lastIndexOf(array[i]);
        if (firstOccurrence === lastOccurrence || lastOccurrence === i) {
          toReturn.push(array[i]);
        }
      }
      return toReturn;
    }

    return service;
  }
})();
