(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('wishListService', wishListService);

  function wishListService(LOCAL_STORAGE_KEYS) {

    // public
    var service = {
      removeProductFromWishList: _removeProductFromWishList,
      addProductToWishList: _addProductToWishList,
      getProductIdsFromWishList: _getProductIdsFromWishList
    };

    // private
    function _getProductIdsFromWishList() {
      var wishListString = localStorage[LOCAL_STORAGE_KEYS.WISH_LIST];
      if (wishListString) {
        return JSON.parse(wishListString);
      }
      return '';
    }

    function _addProductToWishList(productId) {
      if (!productId || typeof productId !== 'string') {
        console.warn('Could not add product from wish list because id is not valid. Id = ');
        console.warn(productId);
        return;
      }
      var wishListString = localStorage[LOCAL_STORAGE_KEYS.WISH_LIST],
        wishListArray;

      if (wishListString) {
        wishListArray = JSON.parse(wishListString);
        if (wishListArray.indexOf(productId) >= 0) {
          console.log('Product already in wish list. Not adding it anymore. Aborting...');
          return;
        }
        wishListArray.push(productId);
        wishListString = JSON.stringify(wishListArray);
        localStorage[LOCAL_STORAGE_KEYS.WISH_LIST] = wishListString;
      } else {
        wishListArray = [];
        wishListArray.push(productId);
        wishListString = JSON.stringify(wishListArray);
        localStorage[LOCAL_STORAGE_KEYS.WISH_LIST] = wishListString;
      }
    }

    function _removeProductFromWishList(productId) {
      if (!productId || typeof productId !== 'string') {
        console.warn('Could not remove product from wish list because id is not valid. Id = ');
        console.warn(productId);
        return;
      }

      var wishListString = localStorage[LOCAL_STORAGE_KEYS.WISH_LIST],
        wishListArray;

      if (!wishListString || wishListString.indexOf(productId) < 0) {
        console.warn('ProductId was not found in the wish list. Could not remove product from wish list. Id = ');
        console.warn(productId);
        return;
      }

      wishListArray = JSON.parse(wishListString);
      wishListArray.splice(wishListArray.indexOf(productId), 1);
      wishListString = JSON.stringify(wishListArray);
      localStorage[LOCAL_STORAGE_KEYS.WISH_LIST] = wishListString;
    }
    return service;
  }
  wishListService.$inject = ['LOCAL_STORAGE_KEYS'];
})();
