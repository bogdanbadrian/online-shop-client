(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('recentViewedProductsService', recentViewedProductsService);

  function recentViewedProductsService($log, LOCAL_STORAGE_KEYS, helperService) {

    // public
    var service = {
      getProducts: _getProducts,
      addProduct: _addProduct
    };

    function _addProduct(productId) {
      if (!productId || typeof productId !== 'string') {
        console.warn('Could not add product as recent viewed because id is not valid. Id = ');
        console.warn(productId);
        return;
      }
      var recentViewedProductsString = localStorage[LOCAL_STORAGE_KEYS.RECENT_VIEWED_PRODUCTS],
        recentViewedProductsArray;

      if (recentViewedProductsString) {
        recentViewedProductsArray = JSON.parse(recentViewedProductsString);
        recentViewedProductsArray.push(productId);
        recentViewedProductsArray = helperService.removeDuplicates(recentViewedProductsArray);
        if (recentViewedProductsArray.length > 5) {
          recentViewedProductsArray.shift();
        }
      } else {
        recentViewedProductsArray = [];
        recentViewedProductsArray.push(productId);
      }
      recentViewedProductsString = JSON.stringify(recentViewedProductsArray);
      localStorage[LOCAL_STORAGE_KEYS.RECENT_VIEWED_PRODUCTS] = recentViewedProductsString;
    }

    function _getProducts() {
      var recentViewedProductsString = localStorage[LOCAL_STORAGE_KEYS.RECENT_VIEWED_PRODUCTS];
      if (recentViewedProductsString) {
        return JSON.parse(recentViewedProductsString);
      }
      return '';
    }

    return service;
  }
  recentViewedProductsService.$inject = ['$log', 'LOCAL_STORAGE_KEYS', 'helperService'];
})();
