(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('cachedProductsService', cachedProductsService);

  function cachedProductsService() {

    // public
    var service = {
      addProduct: _addProduct,
      removeProduct: _removeProduct,
      getProductById: _getProductById
    };

    // private
    var _cachedProducts = [];

    function _addProduct(product) {
      if (!product) {
        console.warn('Product is not valid. Could not add product to cached data.');
        return;
      }
      if (!_productAlreadyInCache(product._id)) {
        _setPictureIndex(product);
        _cachedProducts.push(product);
      }
    }

    function _setPictureIndex(product) {
      if (!product.otherPicturePaths.length || (product.otherPicturePaths[0] && product.otherPicturePaths[0].pictureUrl)) {
        console.warn('Product does not have photos or they are already polished.');
        return;
      }
      var currentPicturePath;
      for (var i = product.otherPicturePaths.length - 1; i >= 0; i--) {
        currentPicturePath = product.otherPicturePaths[i] && product.otherPicturePaths[i].pictureUrl ? product.otherPicturePaths[i].pictureUrl : product.otherPicturePaths[i];
        if (currentPicturePath) {
          product.otherPicturePaths[i] = {
            index: i,
            pictureUrl: currentPicturePath
          };
        } else {
          product.otherPicturePaths.splice(i, 1);
        }
      }
      product.otherPicturePaths.push({
        pictureUrl: product.mainPicturePath,
        index: product.otherPicturePaths.length
      });
    }

    function _removeProduct(productId) {
      if (!productId || typeof productId !== 'string') {
        console.warn('Could not remove product from cached data because id is invalid. Id = ');
        console.warn(productId);
        return;
      }
      if (_productAlreadyInCache(productId)) {
        for (var i = 0; i < _cachedProducts; i++) {
          if (_cachedProducts[i]._id === productId) {
            break;
          }
        }
        _cachedProducts.splice(i, 1);
      }
    }

    function _productAlreadyInCache(productId) {
      return _.find(_cachedProducts, function(currentProduct) {
        return currentProduct._id === productId;
      });
    }

    function _getProductById(productId) {
      var searchedProduct = _productAlreadyInCache(productId);
      if (searchedProduct) {
        return searchedProduct;
      } else {
        console.warn('Could not get product by id from cached products because it does not exist.');
        return;
      }
    }
    return service;
  }
})();
