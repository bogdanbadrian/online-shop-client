(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('applicationService', applicationService);

  function applicationService($http, $log, API) {

    // public
    var service = {
      getLanguages: _getLanguages,
      getCurrentLanguage: _getCurrentLanguage,
      init: _init,
      registerObserverCallback: _registerObserverCallback,
      languagesLoaded: _languagesLoaded,
      searchProducts: _searchProducts,
      getContactData: _getContactData
    };

    // private
    var _currentLanguage,
      observerCallbacks = [],
      _languages,
      _cachedContactData;

    function _getLanguages(successCallback, errorCallback) {
      if (_currentLanguage) {
        if (_.isFunction(successCallback)) {
          successCallback(_languages);
          return;
        }
      }
      $http.get(API.DOMAIN + API.GET_LANGUAGES).success(function(languages) {
          $log.log('Languages received.');
          _setLanguage(languages[0]);
          _languages = languages;
          if (_.isFunction(successCallback)) {
            successCallback(languages);
          }
        })
        .error(function(err) {
          $log.warn('Getting languages could not be possible');
          if (_.isFunction(errorCallback)) {
            errorCallback(err);
          }
        });
    }

    function _getContactData(successCallback, errorCallback) {
      if (_cachedContactData) {
        $log.info('Contact data already exist. Returning cached data...');
        successCallback(_cachedContactData);
        return;
      }

      $http.get(API.DOMAIN + API.ADMIN).success(function(results) {
          $log.info('Contact data received.');
          if (_.isFunction(successCallback)) {
            $log.info('Calling getContactData successCallback.');
            successCallback(results);
          }
        })
        .error(function(err) {
          $log.info('Contact data failed to load.');
          if (_.isFunction(errorCallback)) {
            $log.info('Calling getContactData errorCallback.');
            errorCallback(err);
          }
        });
    }

    function _getCurrentLanguage() {
      return _currentLanguage ? _currentLanguage : null;
    }

    function _init() {
      _getLanguages();
    }

    function _setLanguage(newLanguage) {
      _currentLanguage = newLanguage;
      _notifyObserverCallbacks();
    }

    // OBSERVER PATTERN
    function _registerObserverCallback(callback) {
      if (_.isFunction(callback)) {
        observerCallbacks.push(callback);
      }
    }

    function _notifyObserverCallbacks() {
      for (var i = 0; i < observerCallbacks.length; i++) {
        observerCallbacks[i](_currentLanguage);
      }
    }

    function _languagesLoaded() {
      return !!_currentLanguage;
    }

    function _searchProducts(queryText, successCallback, errorCallback) {
      var searchUrl = API.DOMAIN + API.SEARCH;
      if (queryText) {
        queryText = queryText.toLowerCase();
        queryText = encodeURIComponent(queryText);
        searchUrl += queryText;
      } else {
        $log.warn('Query text is not valid. Aborting search...');
        return;
      }

      $http.get(searchUrl).success(function(searchResults) {
          $log.log('Search results received.');
          if (_.isFunction(successCallback)) {
            successCallback(searchResults);
          }
        })
        .error(function(err) {
          $log.warn('Searching for \'' + queryText + '\' could not be possible.');
          if (_.isFunction(errorCallback)) {
            errorCallback(err);
          }
        });
    }
    return service;
  }
  applicationService.$inject = ['$http', '$log', 'API'];
})();
