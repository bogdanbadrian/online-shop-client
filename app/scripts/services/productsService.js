(function() {
  'use strict';

  angular
    .module('onlineShopV2App')
    .factory('productsService', productsService);

  function productsService($http, API, cachedProductsService, applicationService) {

    // public
    var service = {
      registerObserverCallback: _registerObserverCallback,
      getProducts: _getProducts,
      getProductById: _getProductById,
      updateProduct: _updateProduct,
      deleteProduct: _deleteProduct,
      createProduct: _createProduct,
      getRecommendedProducts: _getRecommendedProducts,
      registerRecommendedProductsObserverCallback: _registerRecommendedProductsObserverCallback
    };
    // private
    var _products = [],
      observerCallbacks = [],
      recommendedProductsObserverCallbacks = [];

    function _getRecommendedProducts(successCallback, errorCallback) {
      $http.get(API.DOMAIN + API.GET_RECOMMENDED_PRODUCTS)
        .success(function(products) {
          _cacheProducts(products);
          products = sortRecommendedProducts(products);
          if (_.isFunction(successCallback)) {
            successCallback(products);
          }
        })
        .error(function(err) {
          console.warn('Could not get recommended products');
          if (_.isFunction(errorCallback)) {
            errorCallback(err);
          }
        });
    }

    function _getProducts(url, successCallback, errorCallback) {
      $http.get(url)
        .success(function(products) {
          console.log('Recommended products have arrived.');
          _cacheProducts(products);
          if (_.isFunction(successCallback)) {
            successCallback(products);
          }
        })
        .error(function(err) {
          console.warn('Could not get products');
          if (_.isFunction(errorCallback)) {
            errorCallback(err);
          }
        });
    }

    function _getProductById(id, successCallback, errorCallback) {
      var url = API.DOMAIN + API.GET_PRODUCT_BY_ID + id;
      $http.get(url)
        .success(function(product) {
          console.log('Product with id=' + id + ' received.');
          _cacheProducts([product]);
          if (_.isFunction(successCallback)) {
            successCallback(cachedProductsService.getProductById(id));
          }
        })
        .error(function(err) {
          console.warn('Could not get product with id=' + id);
          if (_.isFunction(errorCallback)) {
            errorCallback(err);
          }
        });
    }

    function _getAllProducts(successCallback, errorCallback) {
      var url = API.DOMAIN + API.GET_ALL_PRODUCTS;
      $http.get(url)
        .success(function(products) {
          if (_.isFunction(successCallback)) {
            successCallback(products);
          }
        })
        .error(function(err) {
          if (_.isFunction(errorCallback)) {
            errorCallback(err);
          }
        });
    }

    function _cacheProducts(productsArray) {
      for (var i = 0; i < productsArray.length; i++) {
        cachedProductsService.addProduct(productsArray[i]);
      }
    }

    function _updateProduct(product, successCallback, errorCallback) {
      $http.patch(product.links.self, product)
        .success(function(product) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update product because success callback are not functions.');
            return;
          }
          _onUpdate(product);
          successCallback(product);
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update product because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _deleteProduct(product, successCallback, errorCallback) {
      $http.delete(product.links.self)
        .success(function() {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update product because success callback are not functions.');
            return;
          }
          _onDelete(product);
          successCallback();
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update product because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    function _createProduct(product, successCallback, errorCallback) {
      product.language_id = applicationService.getCurrentLanguage()._id;
      $http.post(API.DOMAIN + API.POST_PRODUCTS, product)
        .success(function(product) {
          if (!_.isFunction(successCallback)) {
            console.warn('Could not update product because success callback are not functions.');
            return;
          }
          _onCreate(product);
          successCallback(product);
        })
        .error(function(err) {
          if (!_.isFunction(errorCallback)) {
            console.warn('Could not update product because error callback are not functions.');
            return;
          }
          console.error(err);
          errorCallback(err);
        });
    }

    var _orderProducts = function() {
      _products.sort(function(a, b) {
        if (a.isActive === b.isActive) {
          return a.orderNumber - b.orderNumber;
        } else if (a.isActive) {
          return -1;
        }
        return 1;
      });
    };

    var _orderRecommendedProducts = function() {
      var localProducts;
      if (_products) {
        localProducts = _products.filter(function(a) {
          return a.isRecommended === true;
        });
        localProducts = sortRecommendedProducts(localProducts);
      }
      return localProducts;
    };

    function sortRecommendedProducts(recommendedProducts) {
      return _.isArray(recommendedProducts) && recommendedProducts.length ? recommendedProducts.sort(function(a, b) {
        return a.recommendationOrderNumber - b.recommendationOrderNumber;
      }) : [];
    }

    var _onCreate = function(product) {
      _products.push(product);
      _orderProducts();
      _notifyObserverCallbacks(_products);
      if (product.isRecommended) {
        _notifyRecommendedProductsObserverCallbacks();
      }
    };

    var _onDelete = function(product) {
      for (var i = 0; i < _products.length; i++) {
        if (_products[i]._id === product._id) {
          _products.splice(i, 1);
          break;
        }
      }
      _notifyObserverCallbacks(_products);
      if (product.isRecommended) {
        _notifyRecommendedProductsObserverCallbacks();
      }
      if (product.mainPicturePath && product.mainPicturePath.indexOf(API.UPLOADS) > -1 && product.otherPicturePaths.indexOf(product.mainPicturePath) === -1) {
        _deleteImage(product.mainPicturePath);
      }
      product.otherPicturePaths.forEach(function(element, index, array) {
          if(element) {
            _deleteImage(element);
          }
      });
    };

    var _onUpdate = function(product) {
      /* jshint ignore:start */
      for (var i = 0; i < _products.length; i++) {
        if (_products[i]._id === product._id) {
          if (_products[i].mainPicturePath !== product.mainPicturePath && product.otherPicturePaths.indexOf(_products[i].mainPicturePath) === -1) {
            if (_products[i].mainPicturePath && _products[i].mainPicturePath.indexOf(API.UPLOADS) > -1) {
              _deleteImage(_products[i].mainPicturePath);
            }
          }
          _products[i].otherPicturePaths.forEach(function(element, index, array) {
            if (product.otherPicturePaths.indexOf(element) === -1 && product.mainPicturePath !== element) {
              _deleteImage(element);
            }
          });
          _products[i] = product;
          break;
        }
      }
      /* jshint ignore:end */
      _orderProducts();
      _notifyObserverCallbacks(_products);
      _notifyRecommendedProductsObserverCallbacks();
    };

    var _deleteImage = function(url) {
      $http.delete(url)
        .success(function() {
          console.log('Picture deleted.');
        })
        .error(function(err) {
          console.error(err);
        });
    };

    // OBSERVER PATTERN
    function _registerObserverCallback(callback) {
      if (_.isFunction(callback)) {
        observerCallbacks.push(callback);
      }
    }

    function _notifyObserverCallbacks(data) {
      for (var i = 0; i < observerCallbacks.length; i++) {
        observerCallbacks[i](data);
      }
    }

    function _registerRecommendedProductsObserverCallback(callback) {
      if (_.isFunction(callback)) {
        recommendedProductsObserverCallbacks.push(callback);
      }
    }

    function _notifyRecommendedProductsObserverCallbacks() {
      var data = _orderRecommendedProducts();
      for (var i = 0; i < recommendedProductsObserverCallbacks.length; i++) {
        recommendedProductsObserverCallbacks[i](data);
      }
    }

    function _init() {
      if (_products.length === 0) {
        _getAllProducts(function(products) {
          _products = products;
        });
      }
    }

    _init();

    return service;
  }
  productsService.$inject = ['$http', 'API', 'cachedProductsService', 'applicationService'];
})();
