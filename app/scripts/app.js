'use strict';

angular
    .module('onlineShopV2App', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'ngFileUpload',
        'jkuri.gallery',
        'sortSelect',
        'search-box',
        'offClick',
        'vcRecaptcha'
    ])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('shop', {
                abstract: true,
                url: '/',
                templateUrl: 'views/shop-layout.html',
                controller: 'mainCtrl'
            })
            .state('shop.home', {
                url: 'home',
                templateUrl: 'views/main.html',
            })
            .state('shop.category', {
                url: 'categories/:selectedCategory',
                templateUrl: 'views/category.html',
                controller: 'categoryCtrl'
            })
            .state('shop.product', {
                url: 'products/:selectedProduct',
                templateUrl: 'views/product.html',
                controller: 'productCtrl'
            })
            .state('shop.search', {
                url: 'search/:queryText',
                templateUrl: 'views/search.html',
                controller: 'searchCtrl'
            })
            .state('shop.about', {
                url: 'about',
                templateUrl: 'views/about.html',
            })
            .state('shop.contact', {
                url: 'contact',
                templateUrl: 'views/contact.html',
                controller: 'contactCtrl'
            })
            .state('shop.wishList', {
                url: 'wishList',
                templateUrl: 'views/wishList.html',
                controller: 'wishListCtrl'
            })
            .state('admin', {
                url: '/admin',
                controller: 'adminCtrl',
                templateUrl: 'views/login.html'
            })
            .state('administration', {
                controller: 'adminCtrl',
                templateUrl: 'views/admin/admin-layout.html'
            });
        $urlRouterProvider.otherwise('/home');
    })
    .run(function(applicationService, categoriesService) {
        applicationService.init();
        categoriesService.init();
    });
