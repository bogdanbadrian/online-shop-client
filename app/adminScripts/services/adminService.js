'use strict';

angular.module('onlineShopV2App')
    .factory('adminService', ['$http', 'API', function($http, API) {

        var _admin = {},
            _isAuthenticated = false;

        var _login = function(password, successCallback, errorCallback) {
            $http.post(API.DOMAIN + API.ADMIN, { password: password }).then(
                function(result) {
                    if (_.isFunction(successCallback)) {
                        _admin = result.data;
                        _isAuthenticated = true;
                        successCallback(_admin);
                    }
                },
                function(err) {
                    console.warn('Could not get the admin password');
                    console.warn(err);
                    if (_.isFunction(errorCallback)) {
                        _isAuthenticated = false;
                        errorCallback(err);
                    }
                }
            );
        };

        var _getAdminData = function(successCallback) {
            if (_.isFunction(successCallback)) {
                successCallback(_admin);
            }
        };

        var _updateAdmin = function(admin, successCallback, errorCallback) {
            $http.patch(admin.links.self, admin)
                .success(function(result) {
                    if (!_.isFunction(successCallback)) {
                        console.warn('Could not update admin because success callback are not functions.');
                        return;
                    }
                    _admin = result;
                    successCallback(_admin);
                })
                .error(function(err) {
                    if (!_.isFunction(errorCallback)) {
                        console.warn('Could not update admin because error callback are not functions.');
                        return;
                    }
                    errorCallback(err);
                });
        };

        var _isAuthorized = function(successCallback) {
            successCallback(_isAuthenticated);
        };

        var _sendPasswordMailToAdmin = function(successCallback, errorCallback) {
            $http.post(API.DOMAIN + API.SEND_MAIL_TO_ADMIN).then(
                function(result) {
                    if (_.isFunction(successCallback)) {
                        successCallback(result);
                    }
                },
                function(err) {
                    if (_.isFunction(errorCallback)) {
                        errorCallback(err);
                    }
                }
            );
        };

        var _sendPasswordMailToOthers = function(successCallback, errorCallback) {
            $http.post(API.DOMAIN + API.SEND_MAIL_TO_OTHERS).then(
                function(result) {
                    if (_.isFunction(successCallback)) {
                        successCallback(result);
                    }
                },
                function(err) {
                    if (_.isFunction(errorCallback)) {
                        errorCallback(err);
                    }
                }
            );
        };

        var _getImageUrls = function(successCallback, errorCallback) {
            $http.get(API.UPLOADS).then(function(result) {
                if (_.isFunction(successCallback)) {
                    successCallback(result);
                }
            }, function(err) {
                if (_.isFunction(errorCallback)) {
                    errorCallback(err);
                }
            });
        };

        var _deleteImage = function(url, successCallback) {
            $http.delete(url)
                .success(function() {
                  successCallback();
                })
                .error(function(err) {
                    console.error(err);
                });
        };


        return {
            login: _login,
            getAdminData: _getAdminData,
            updateAdmin: _updateAdmin,
            isAuthorized: _isAuthorized,
            sendPasswordMailToAdmin: _sendPasswordMailToAdmin,
            sendPasswordMailToOthers: _sendPasswordMailToOthers,
            getImageUrls: _getImageUrls,
            deleteImage: _deleteImage
        };

    }]);
