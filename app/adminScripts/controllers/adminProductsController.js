'use strict';

angular.module('onlineShopV2App')
  .controller('adminProductsController', ['$scope', 'applicationService', 'productsService', 'categoriesService', function($scope, applicationService, productsService, categoriesService) {

    $scope.modalShown = false;
    $scope.displayCreateButton = false;
    $scope.selectedProduct = {};
    $scope.selectedCategory = {};

    $scope.onSelectedCategoryChanged = function(category) {
      $scope.selectedCategory = category;
      $scope.displayCreateButton = true;
      $scope.products = category.products;
    };

    $scope.onToggleModal = function(product) {
      $scope.modalShown = !$scope.modalShown;
      if (!$scope.modalShown) {
        $scope.selectedProduct = {
          otherPicturePaths: new Array(5)
        };
      } else
        $scope.selectedProduct = product ? jQuery.extend(true, {}, product) : {
          otherPicturePaths: new Array(5)
        };
      if (!$scope.selectedCategory._id && product)
        $scope.selectedCategory = $scope.categories.filter(function(category) {
          return category._id === product.category_id;
        })[0];
    };

    var _init = function() {
        _getCategories();
        _subscribeToCategoriesChanged();
        _subscribeToProductsChanged();
        _getRecommendedProducts();
        _subscribeToRecommendedProductsChanged();
      },
      _getCategories = function() {
        categoriesService.getCategories(function(categories) {
          $scope.categories = categories;
          for (var i = 0; i < $scope.categories.length; i++) {
            _getProducts(i);
          }
        });
      },
      _getProducts = function(i) {
        productsService.getProducts($scope.categories[i].links.products, function(products) {
          $scope.categories[i].products = [];
          $scope.categories[i].products = products;
        });
      },
      _subscribeToCategoriesChanged = function() {
        categoriesService.onCategoriesLoaded(function(categories) {
          $scope.categories = categories;
          for (var i = 0; i < $scope.categories.length; i++) {
            _getProducts(i);
          }
        });
      },
      _subscribeToProductsChanged = function() {
        productsService.registerObserverCallback(function(products) {
          for (var i = 0; i < $scope.categories.length; i++) {
            $scope.categories[i].products = products.filter(function(product) {
              return product.category_id === $scope.categories[i]._id;
            });
            if ($scope.categories[i]._id === $scope.selectedCategory._id)
              $scope.products = $scope.categories[i].products;
          }
        });
      },
      _getRecommendedProducts = function() {
        productsService.getRecommendedProducts(function(recommendedProducts) {
          $scope.recommendedProducts = recommendedProducts;
        });
      },
      _subscribeToRecommendedProductsChanged = function() {
        productsService.registerRecommendedProductsObserverCallback(function(recommendedProducts) {
          $scope.recommendedProducts = recommendedProducts;
        });
      };

    _init();
  }]);
