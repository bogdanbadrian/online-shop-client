'use strict';

angular.module('onlineShopV2App')
  .controller('adminCategoriesController', ['$scope', '$rootScope', 'applicationService', 'categoriesService', function($scope, $rootScope, applicationService, categoriesService) {

    $scope.modalShown = false;
    $scope.selectedCategory = {};

    $scope.onToggleModal = function(category) {
      $scope.modalShown = !$scope.modalShown;
      if (!$scope.modalShown)
        $scope.selectedCategory = {};
      else
        $scope.selectedCategory = category ? jQuery.extend(true, {}, category) : null;
    }

    $scope.onCreateOrUpdate = function() {
      $scope.modalShown = false;
      $scope.selectedCategory = {};
    }

    var _init = function() {
        if (!applicationService.getCurrentLanguage()) {
          applicationService.registerObserverCallback(_onLanguageLoaded);
        } else {
          _getCategories();
          _subscribeToCategoriesChanged();
        }
      },
      _onLanguageLoaded = function() {
        _getCategories();
      },
      _getCategories = function() {
        categoriesService.getCategories(function(categories) {
          $scope.categories = categories;
        });
      },
      _subscribeToCategoriesChanged = function() {
        categoriesService.onCategoriesLoaded(function(categories) {
          $scope.categories = categories;
        });
      };

    _init();
  }]);
