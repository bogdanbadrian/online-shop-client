'use strict';

angular.module('onlineShopV2App')
  .controller('adminBannersController', ['$scope', 'applicationService', 'bannersService', function($scope, applicationService, bannersService) {

    $scope.modalShown = false;
    $scope.selectedBanner = {};

    $scope.onToggleModal = function(banner) {
      $scope.modalShown = !$scope.modalShown;
      if (!$scope.modalShown)
        $scope.selectedBanner = {};
      else
        $scope.selectedBanner = banner ? jQuery.extend(true, {}, banner) : null;
    };

    var _init = function() {
        if (!applicationService.getCurrentLanguage()) {
          applicationService.registerObserverCallback(_onLanguageLoaded);
        } else {
          _getBanners();
          _subscribeToBannersChanged();
        }
      },
      _onLanguageLoaded = function() {
        _getBanners();
      },
      _getBanners = function() {
        bannersService.getBanners(function(banners) {
          $scope.banners = banners;
        });
      },
      _subscribeToBannersChanged = function() {
        bannersService.registerObserverCallback(function(banners) {
          $scope.banners = banners;
        });
      };

    _init();
  }]);
