'use strict';

angular.module('onlineShopV2App')
  .directive('uploadImage', function() {
    return {
      restrict: 'E',
      scope: {
        path: '='
      },
      replace: true, // Replace with the template below
      templateUrl: 'adminDirectives/uploadImage/upload-image.html',
      controller: 'uploadImageController',
      link: function(scope, element, attrs) {
        scope.title = attrs.title;
      }
    }
  }).controller('uploadImageController', ['$scope', '$state', '$timeout', 'Upload', 'API', function($scope, $state, $timeout, Upload, API) {
    $scope.uploadFiles = function(file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: API.UPLOADS,
          data: {
            file: file
          }
        });

        file.upload.then(function(response) {
          $timeout(function() {
            if (response.status === 200)
              if (!$scope.product)
                $scope.product = {};

            var imageUrl = API.UPLOADS + file.name;
            $scope.path = imageUrl; 
          });
        }, function(response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function(evt) {
          file.progress = Math.min(100, parseInt(100.0 *
            evt.loaded / evt.total));
        });
      }
    };

  }]);
