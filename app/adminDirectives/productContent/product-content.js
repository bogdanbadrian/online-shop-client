'use strict';

angular.module('onlineShopV2App')
  .directive('productContent', function() {
    return {
      restrict: 'E',
      scope: {
        show: '=',
        selectedCategory: '=',
        categories: '=',
        product: '=',
        toggleAdminModal: '&'
      },
      replace: true, // Replace with the template below
      link: function(scope, element, attrs) {
        scope.dialogStyle = {};
        if (attrs.width)
          scope.dialogStyle.width = attrs.width;
        if (attrs.height)
          scope.dialogStyle.height = attrs.height;
        scope.hideModal = function() {
          scope.show = false;
          scope.product = {};
          scope.product.mainPicturePath = "";
          scope.showErrorMessageForName = false;
          scope.showErrorMessageForMainPicturePath = false;
          scope.showErrorMessageForOrderNumber = false;
          scope.showErrorMessageForOldPrice = false;
          scope.showErrorMessageForPrice = false;
          scope.toggleModal();
        };
      },
      templateUrl: 'adminDirectives/productContent/product-content.html',
      controller: 'productContentController'
    }
  }).controller('productContentController', ['$scope', '$state', '$timeout', 'productsService', 'API', function($scope, $state, $timeout, productsService, API) {
    $scope.showErrorMessageForMainPicturePath = false;
    $scope.showErrorMessageForName = false;
    $scope.showErrorMessageForOrderNumber = false;
    $scope.showErrorMessageForOldPrice = false;
    $scope.showErrorMessageForPrice = false;

    $scope.changeProductCategory = function(category_id) {
      $scope.product.category_id = category_id;
    };

    $scope.toggleModal = function() {
      $scope.toggleAdminModal($scope.product);
    };

    $scope.saveProduct = function() {
      if (_dataIsValid()) {
        $scope.product.category_id = $scope.selectedCategory._id;
        if ($scope.product._id) {
          productsService.updateProduct($scope.product, function(product) {
            $timeout(function() {
              $scope.toggleModal();
            });
          }, function() {});
        } else {
          productsService.createProduct($scope.product, function(product) {
            $timeout(function() {
              $scope.toggleModal();
            });
          });
        };
      }
    };

    var _dataIsValid = function() {
      if (!$scope.product) {
        $scope.showErrorMessageForMainPicturePath = true;
        $scope.showErrorMessageForName = true;
        $scope.showErrorMessageForOrderNumber = true;
        $scope.showErrorMessageForOldPrice = true;
        $scope.showErrorMessageForPrice = true;
        return false;
      } else {
        $scope.showErrorMessageForMainPicturePath = $scope.product.mainPicturePath ? false : true;
        $scope.showErrorMessageForName = $scope.product.name ? false : true;
        $scope.showErrorMessageForOrderNumber = $scope.product.orderNumber ? isNaN($scope.product.orderNumber) : true;
        $scope.showErrorMessageForOldPrice = $scope.product.oldPrice ? isNaN($scope.product.oldPrice) : true;
        $scope.showErrorMessageForPrice = $scope.product.price ? isNaN($scope.product.price) : true;
        return !$scope.showErrorMessageForName && 
               !$scope.showErrorMessageForMainPicturePath && 
               !$scope.showErrorMessageForOrderNumber && 
               !$scope.showErrorMessageForOldPrice && 
               !$scope.showErrorMessageForPrice;
      }
    };
  }]);
