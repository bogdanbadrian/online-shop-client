'use strict';

angular.module('onlineShopV2App')
  .directive('bannerContent', function() {
    return {
      restrict: 'E',
      scope: {
        show: '=',
        banner: '=',
        toggleAdminModal: '&'
      },
      replace: true, // Replace with the template below
      link: function(scope, element, attrs) {
        scope.dialogStyle = {};
        if (attrs.width)
          scope.dialogStyle.width = attrs.width;
        if (attrs.height)
          scope.dialogStyle.height = attrs.height;
        scope.hideModal = function() {
          scope.show = false;
          scope.banner = {};
          scope.banner.picturePath = "";
          scope.showErrorMessageForName = false;
          scope.showErrorMessageForPicturePath = false;
          scope.showErrorMessageForOrderNumber = false;
          scope.toggleModal();
        };
      },
      templateUrl: 'adminDirectives/bannerContent/banner-content.html',
      controller: 'bannerContentController'
    }
  }).controller('bannerContentController', ['$scope', '$state', '$timeout', 'bannersService', 'Upload', 'API', function($scope, $state, $timeout, bannersService, Upload, API) {
    $scope.showErrorMessageForPicturePath = false;
    $scope.showErrorMessageForName = false;
    $scope.showErrorMessageForOrderNumber = false;

    $scope.toggleModal = function() {
      $scope.toggleAdminModal($scope.banner);
    };

    $scope.uploadFiles = function(file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: API.UPLOADS,
          data: {
            file: file
          }
        });

        file.upload.then(function(response) {
          $timeout(function() {
            if (response.status === 200)
              if (!$scope.banner)
                $scope.banner = {};
            $scope.banner.picturePath = API.UPLOADS + file.name;
          });
        }, function(response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data;
        }, function(evt) {
          file.progress = Math.min(100, parseInt(100.0 *
            evt.loaded / evt.total));
        });
      }
    };

    $scope.saveBanner = function() {
      if (_dataIsValid()) {
        if ($scope.banner._id) {
          bannersService.updateBanner($scope.banner, function(banner) {
            $timeout(function() {
              $scope.toggleModal();
            });
          }, function() {});
        } else {
          bannersService.createBanner($scope.banner, function(banner) {
            $timeout(function() {
              $scope.toggleModal();
            });
          });
        };
      }
    };

    var _dataIsValid = function() {
      if (!$scope.banner) {
        $scope.showErrorMessageForPicturePath = true;
        $scope.showErrorMessageForName = true;
        $scope.showErrorMessageForOrderNumber = true;
        return false;
      } else {
        $scope.showErrorMessageForPicturePath = $scope.banner.picturePath ? false : true;
        $scope.showErrorMessageForName = $scope.banner.name ? false : true;
        $scope.showErrorMessageForOrderNumber = $scope.banner.orderNumber ? isNaN($scope.banner.orderNumber) : true;
        return !$scope.showErrorMessageForName && !$scope.showErrorMessageForPicturePath && !$scope.showErrorMessageForOrderNumber;
      }
    };
  }]);
