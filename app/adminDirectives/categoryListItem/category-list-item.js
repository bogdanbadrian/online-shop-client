'use strict';

angular.module('onlineShopV2App')
    .directive('categoryListItem', function() {
        return {
            restrict: 'E',
            scope: {
                category: '=',
                toggleAdminModal: '&'
            },
            templateUrl: 'adminDirectives/categoryListItem/category-list-item.html',
            controller: 'adminCategoryController'
        }
    })
    .controller('adminCategoryController', ['$scope', '$timeout', 'categoriesService', function($scope, $timeout, categoriesService) {
        $scope.showDeleteConfirmation = false;
        $scope.showActionLinks = true;
        $scope.hasProducts = false;

        $scope.toggleModal = function() {
            $scope.toggleAdminModal($scope.category);
        }

        $scope.toggleDeleteConfirmation = function() {
            $scope.showDeleteConfirmation = !$scope.showDeleteConfirmation;
            $scope.showActionLinks = !$scope.showActionLinks;
        }

        $scope.deleteCategory = function() {
            categoriesService.getCategoryProducts($scope.category, function(products) {
                if (products.length == 0) {
                    $scope.hasProducts = false;
                    categoriesService.deleteCategory($scope.category, function() {
                    });
                } else {
                    $scope.showDeleteConfirmation = false;
                    $scope.hasProducts = true;
                    $timeout(function() {
                        $scope.showDeleteConfirmation = false;
                        $scope.showActionLinks = true;
                        $scope.hasProducts = false;
                    }, 5000);
                }
            });
        };
    }]);
