'use strict';

angular.module('onlineShopV2App')
  .directive('categoryContent', function() {
    return {
      restrict: 'E',
      scope: {
        show: '=',
        category: '=',
        toggleAdminModal: '&'
      },
      replace: true, // Replace with the template below
      link: function(scope, element, attrs) {
        scope.dialogStyle = {};
        if (attrs.width)
          scope.dialogStyle.width = attrs.width;
        if (attrs.height)
          scope.dialogStyle.height = attrs.height;
        scope.hideModal = function() {
          scope.show = false;
          scope.category = {};
          scope.showErrorMessageForName = false;
          scope.showErrorMessageForOrderNumber = false;
          scope.toggleModal();
        };
      },
      templateUrl: 'adminDirectives/categoryContent/category-content.html',
      controller: 'categoryContentController'
    }
  }).controller('categoryContentController', ['$scope', '$state', '$timeout', 'categoriesService', 'Upload', 'API', function($scope, $state, $timeout, categoriesService, Upload, API) {
    $scope.showErrorMessageForName = false;
    $scope.showErrorMessageForOrderNumber = false;

    $scope.toggleModal = function() {
      $scope.toggleAdminModal($scope.banner);
    };

    $scope.saveCategory = function() {
      if(_dataIsValid()) {
        if ($scope.category._id) {
          categoriesService.updateCategory($scope.category, function() {
            $timeout(function() {
              $scope.toggleModal();
            });
          }, function() {});
        } else {
          categoriesService.createCategory($scope.category, function() {
            $timeout(function() {
              $scope.toggleModal();
            });
          });
        };
      }
    };

    var _dataIsValid = function() {
      if(!$scope.category) {
        $scope.showErrorMessageForName = true;
        $scope.showErrorMessageForOrderNumber = true;
        return false;
      }
      else {  
        $scope.showErrorMessageForName = $scope.category.name ? false : true;
        $scope.showErrorMessageForOrderNumber = $scope.category.orderNumber ? isNaN($scope.category.orderNumber) : true; 
        return !$scope.showErrorMessageForName && !$scope.showErrorMessageForOrderNumber; 
      }
    };
  }]);
