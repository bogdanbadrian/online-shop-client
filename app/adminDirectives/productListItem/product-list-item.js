'use strict';

angular.module('onlineShopV2App')
  .directive('productListItem', function() {
    return {
      restrict: 'E',
      scope: {
        product: '=',
        toggleAdminModal: '&'
      },
      templateUrl: 'adminDirectives/productListItem/product-list-item.html',
      controller: 'adminProductController'
    }
  })
  .controller('adminProductController', ['$scope', '$timeout', 'productsService', function($scope, $timeout, productsService) {
    $scope.showDeleteConfirmation = false;
    $scope.showActionLinks = true;

    $scope.toggleModal = function() {
      $scope.toggleAdminModal($scope.product);
    }

    $scope.toggleDeleteConfirmation = function() {
      $scope.showDeleteConfirmation = !$scope.showDeleteConfirmation;
      $scope.showActionLinks = !$scope.showActionLinks;    
    }

    $scope.deleteProduct = function() {
      productsService.deleteProduct($scope.product, function() {
      });
    };
  }]);
