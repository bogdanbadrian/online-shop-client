'use strict';

angular.module('onlineShopV2App')
  .directive('recommendedProductListItem', function() {
    return {
      restrict: 'E',
      scope: {
        product: '=',
        selectedCategory: '=',
        toggleAdminModal: '&'
      },
      templateUrl: 'adminDirectives/recommendedProductListItem/recommended-product-list-item.html',
      controller: 'adminRecommendedProductController'
    }
  })
  .controller('adminRecommendedProductController', ['$scope', '$timeout', 'productsService', function($scope, $timeout, productsService) {

    $scope.toggleModal = function() {
      $scope.toggleAdminModal($scope.product);
    }

  }]);
