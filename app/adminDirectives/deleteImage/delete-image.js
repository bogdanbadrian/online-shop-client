'use strict';

angular.module('onlineShopV2App')
  .directive('deleteImage', function() {
    return {
      restrict: 'E',
      scope: {
        path: '='
      },
      replace: true, // Replace with the template below
      templateUrl: 'adminDirectives/deleteImage/delete-image.html',
      controller: 'deleteImageController',
      link: function(scope, element, attrs) {
        scope.title = attrs.title;
        scope.remove = function() {
          element.remove();
        }
      }
    }
  }).controller('deleteImageController', ['$scope', '$state', '$timeout', 'adminService', 'API', function($scope, $state, $timeout, adminService, API) {
    
    $scope.deleteFile = function() {
      adminService.deleteImage($scope.path, function() {
        $scope.remove();
      });
    }; 

  }]);
