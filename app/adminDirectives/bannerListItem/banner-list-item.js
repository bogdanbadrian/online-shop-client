'use strict';

angular.module('onlineShopV2App')
  .directive('bannerListItem', function() {
    return {
      restrict: 'E',
      scope: {
        banner: '=',
        toggleAdminModal: '&',
        onToggleState: '&'
      },
      templateUrl: 'adminDirectives/bannerListItem/banner-list-item.html',
      controller: 'adminBannerController'
    }
  })
  .controller('adminBannerController', ['$scope', '$timeout', 'bannersService', function($scope, $timeout, bannersService) {
    $scope.showDeleteConfirmation = false;
    $scope.showActionLinks = true;

    $scope.toggleModal = function() {
      $scope.toggleAdminModal($scope.banner);
    }

    $scope.toggleState = function() {
      $scope.banner.isActive = !$scope.banner.isActive;
      bannersService.updateBanner($scope.banner, function() {
      }, function() {
        $scope.banner.isActive = !$scope.banner.isActive;  
      });
    };

    $scope.toggleDeleteConfirmation = function() {
      $scope.showDeleteConfirmation = !$scope.showDeleteConfirmation;
      $scope.showActionLinks = !$scope.showActionLinks;    
    }

    $scope.deleteBanner = function() {
      bannersService.deleteBanner($scope.banner, function() {
      });
    };

    $scope.stateText = function() {
      return $scope.banner.isActive ? 'Dezactiveaza' : 'Activeaza';
    };
  }]);
