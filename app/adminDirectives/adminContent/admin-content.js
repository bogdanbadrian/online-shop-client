'use strict';

angular.module('onlineShopV2App')
  .directive('adminContent', function() {
    return {
      restrict: 'E',
      scope: {},
      replace: true, // Replace with the template below
      templateUrl: 'adminDirectives/adminContent/admin-content.html',
      controller: 'adminContentController'
    }
  }).controller('adminContentController', ['$scope', '$state', '$timeout', 'adminService', 'API', function($scope, $state, $timeout, adminService, API) {

    $scope.showErrorMessageForPassword = false;
    $scope.showWarningMessageForPassword = false;
    $scope.showWarningMessageForInfo = false;
    $scope.newPassword = { 
      password: "",
      confirmed: ""
  };

    $scope.changeInfo = function() {
      if (_infoIsValid()) {
        if ($scope.admin._id) {
          adminService.updateAdmin($scope.admin, function() {
            $scope.showWarningMessageForInfo = true;
            $timeout(function(){
              $scope.showWarningMessageForInfo = false;
            }, 5000);
          }, function() {});
        }
      }
    };

    $scope.changePassword = function() {
      if (_passwordIsValid()) {
        if ($scope.admin._id) {
          adminService.updateAdmin({
              password: $scope.newPassword.password,
              links: $scope.admin.links
            }, function() {
            $scope.newPassword.password = "";
            $scope.newPassword.confirmed = "";
            $scope.showWarningMessageForPassword = true;
            $timeout(function(){
              $scope.showWarningMessageForPassword = false;
            }, 5000);
          }, function() {});
        }
      }
    };

    var _infoIsValid = function() {
      return true;
    };

    var _passwordIsValid = function() {
      if (!$scope.admin) {
        $scope.showErrorMessageForPassword = true;
        return false;
      } else {
        $scope.showErrorMessageForPassword = $scope.newPassword.password && $scope.newPassword.confirmed ? $scope.newPassword.password !== $scope.newPassword.confirmed : true;
        return !$scope.showErrorMessageForPassword;
      }
    };

    var _init = function() {
      adminService.getAdminData(function(admin) {
        $scope.admin = admin;
      });
    };

    _init();
  }]);
